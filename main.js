const Promise = require("bluebird");
const AppDAO = require("./data-access-object");
const ProjectRepository = require("./project_repository");
const TaskRepository = require("./task_repository");

const main = () => {
  
  const dbfile = './database.sqlite3'; 
  const dao = new AppDAO( dbfile );

  const genericProjectData = {
    name: "Go chase parked cars on the freeway."
  };

  const fanClubProjectData = {
    name: "Start the Kenny Loggins fanclub if one does not exist."
  };

  const acmeProjectData = {
    name: "Shave my head, then, jump out of a plane without a parachute"
  };

  const blogProjectData = {
    name: "Complete the Node.js SQLite Exercise"
  };

  const projectRepo = new ProjectRepository(dao);
  const taskRepo = new TaskRepository(dao);
  let projectId;

  // dao.voltron();
  // Putting the Data Access Code to Use
  /* taskRepo.leTest().then( (res) => { console.log( "res:" , res );} ); */
  
  projectRepo.createTable()
  .then( () => { return taskRepo.createTable(); } )
  .then( () => { return projectRepo.create( genericProjectData.name ); } )
  .then( () => { return projectRepo.create( fanClubProjectData.name ); } )
  .then( () => { return projectRepo.create( acmeProjectData.name ); } )
  .then( () => { return projectRepo.create( blogProjectData.name ); } )
  // .then( ( data ) => { console.log( "main.js >>> data\n" , data ); } )
  .then( () => {
    
    let projectPromise = projectRepo.getAll();
    let projects = projectPromise.then( ( data ) => {
      // console.log( "data" , data );
      return data;
    } );
    return projects;

  } )
  .then( ( res ) => {
    
    // console.log( "res" , res );

    let lastArrItem = res.slice( res.length - 1 , res.length );
    // console.log( "lastArrItem" , lastArrItem );
    
    projectId = lastArrItem[0].id;

    // console.log( "projectId: " , projectId );

    const tasks = [ { name: 'Outline', description: 'Perform high level overview of different sections', isComplete: 1, projectId } , { name: 'Write', description: 'Compose article content and code examples', isComplete: 0, projectId } ];

    let bigProm = Promise.all( tasks.map( ( task ) => {
      const { name,description,isComplete,projectId } = task;
      return taskRepo.create( name,description,isComplete,projectId );
    } ) );

    return { bigProm , projectId };

  } )
  .then( ( bfres ) => {
    
    // console.log( "main.js >>> bfres\n" , bfres );
    // console.log( "main.js >>> bfres.projectId\n" , bfres.projectId ); 

    let allProjByIdPromise = projectRepo.getById( bfres.projectId );

    let project = allProjByIdPromise.then( (data) => {
      // console.log( ".then() >>> data" , data );
      return data;
    } );

    return project;

  } )
  .then( ( project ) => {
    
    // console.log('main.js >>> project \n' , project );
    // console.log(`project id = ${ project.id }`);
    // console.log(`project name = ${ project.name }`);

    let tasks = projectRepo.getTasks( project.id );
    return tasks;

  } ) 
  .then( ( tasks ) => {
    
    console.log(`Retrieved tasks from DB: \n`);
    
    // tasks = '123abc'; console.log( "tasks: " , tasks );

    let yesno = Array.isArray( tasks );
    return new Promise( ( resolve, reject ) => {
      
      if( yesno === false ){
        reject("'tasks' variable is not an array object");
      }
      else
      if( yesno === true ){
        
        tasks.forEach( ( task ) => {
          console.log(`task id = ${task.id}`);
          console.log(`task name = ${task.name}`);
          console.log(`task description = ${task.description}`);
          console.log(`task isComplete = ${task.isComplete}`);
          console.log(`task projectId = ${task.projectId}`);
          console.log(`>>>> >>>> >>>> \n`);
        } );

        resolve("Success, baby!");
      }

    });
    
  } )
  .then( ( final_answer ) => {
    console.log( "final_answer" , final_answer );
  } )
  .catch( ( err ) => {
    console.log( "Error: \n" , JSON.stringify( err, null, 2) );
  } ); 

};

main();