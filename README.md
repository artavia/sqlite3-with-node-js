# SQLite3 with NodeJS

## Description
Basic lesson with sqlite3 package.

### Processing and/or completion date(s)
June 21, 2020

## Creative Motivation
This is a rehash of a write-up found at stackabuse called [A SQLite Tutorial with Node.js](https://stackabuse.com/a-sqlite-tutorial-with-node-js/ "link to stackabuse").

## The only difference that I can recall...
I think there are none. 

## Lessons I took with me
Although, I changed nothing it did provide motivation for other future projects on how to compose file structure and/or separate code according to their own roles so that I would not be subjected to having everything piled into one file. 

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the 
[ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial"). In order to understand an error I encountered in **that** lesson, I need to understand SQLite3 and, especially, how all of the moving parts work together in **that other lesson**. SQLite3 is one of those **minor subjects** I had to get acquainted with real fast.

Help me if you are able. Otherwise, what some of you mean for evil against me God will take for good and render your evil deeds impotent. 

## God bless!