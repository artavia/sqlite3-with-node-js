// const sqlite3 = require("sqlite3");
const sqlite3 = require("sqlite3").verbose();
const Promise = require("bluebird");

class AppDAO {
  constructor( dbFilePath ){

    this.db = new sqlite3.Database( dbFilePath , (err) => {
      if(err){
        console.log("Could not connect to database:\n" , err );
      }
      console.log("Connected to database.");      
    } ); // This directive CREATES a whole new .db instance

  }
  
  voltron() {
    return new Promise( ( resolve, reject ) => {
      // reject("OH, NO!!!");
      resolve("konichiwa, voltron!!!");
    } );
    
  };

  run( sql, params = [] ){
    return new Promise( ( resolve, reject ) => {
      this.db.run( sql, params, (err) => {
        if(err){
          console.log('Error running sql ' + sql);
          console.log(err);
          reject(err);
        }
        
        // resolve( { id: this.lastID } ); // NOPE

        // resolve( { id: this.id } ); // do not use this
        
        resolve( { id: this } ); // USE THIS!!! 
        
      } );
    } );
  }

  get( sql, params = [] ){
    return new Promise( ( resolve, reject ) => {
      this.db.get( sql, params, ( err, result ) => {
        if(err){
          console.log('Error running sql ' + sql);
          console.log(err);
          reject(err);
        }
        resolve( result );
      } );
    } );
  }

  all( sql, params = [] ){
    return new Promise( ( resolve, reject ) => {
      this.db.all( sql, params, ( err, rows ) => {
        if(err){
          console.log('Error running sql ' + sql);
          console.log(err);
          reject(err);
        }
        resolve( rows );
      } );
    } );
  }
  
};

module.exports = AppDAO;